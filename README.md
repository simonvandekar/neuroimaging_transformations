# README #


#### DISCLAIMER ####
Copyright (c) 2017, Simon Vandekar, UNIVERSITY OF PENNSYLVANIA
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.

#### DESCRIPTION ####

This is code for the manuscript "Faster family-wise error control for neuroimaging with a parametric bootstrap," submitted to Biostatistics on 2017/01/03. For transparency, all of the code is provided with the manuscript and is free for use and modification, however, the processed data used in the manuscript are not publicly available. The base code is R, but several other software packages are needed, including FSL and "convert3d" (c3d from ITKsnap).

Simulated region of interest (ROI) data to run the CBF analyses are in ./manuscript_data. This includes the study specific label image which is used to generate the statistcal map used to make Figure 4 of the manuscript. To run the CBF analyses uncomment the line "pncname = file.path(progsdir, '../manuscript_data/fakedata_20170807.rds')."

#### OVERVIEW OF CODE ####
All code is included in the "progs" directory.
The simulation analyses were run with "master.R" and "grid.R". The master file submits each simulation to the cluster (LSF). The grid script runs the analyses for a given simulation. Sample sizes, smoothing, and voxel or roi analyses can be specified in the master.R script.

functions.R. These are the functions necessary to run the voxelwise analyses:
PBsim -- this runs one simulation analyses for the PBJ MTP.
PBadjust -- this is the function to perform the PBJ correction procedure.
randomise -- this is a wrapper to run the FSL program "randomise."
readNIfTIs -- this reads in nifti images of the real imaging data, but adds in signal for the simulation analyses.
compile -- this is run after the simulations are completed to compile the results of the simulations.
roicompile -- this is the same thing for the ROI data, which is output in a slightly different format.
randomisecompile -- compiles the results for the randomise output.
normalCI -- creates CI for normal data.
pcompute -- computes p-values for the simulation analyses. Called by PBsim.
randomisetest -- runs randomise for the simulation analyses. Cleans up output for compile script.

roifunctions.R. These are roi versions of the other code. Output is slightly different.

aggregate.R. This compiles the results from the voxel-wise simulation analyses.

roi_aggregate.R. Same except for the roi data.

pnc_analysis.R. Runs the voxel-wise analysis of the CBF data.

roi_pnc_analysis.R. Runs the roi analyses on the region-wise CBF data.

pb.cpp. C++ code to run the parametric bootstrap procedure.

YJtransform.R. Estimates the YJ transformation at each location in the image. Runs in parallel.

The synth_sim directory includes the code to run the synthetic simulation analyses performed in the paper.
